package core.server;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.google.inject.Binding;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;

import core.api.Remote;
import core.remote.call.RemoteBinding;
import core.server.remote.RemoteBindingList;

class Server extends AbstractExecutionThreadService {

	private ServerSocket socket;
	private final ClientFactory client;
	private final RemoteBindingList remoteBindings;
	
	private volatile boolean enabled = true;
	
	private final ExecutorService executor = Executors.newFixedThreadPool(5);

	@Inject public Server(ClientFactory factory, RemoteBindingList rbList, Injector injector) {
		client = factory;
		remoteBindings = rbList;
		
		//dirty hack to build a remote binding list
		for(Entry<Key<?>, Binding<?>> entry  : injector.getAllBindings().entrySet()) {
			if(entry.getKey().getAnnotationType() == Remote.class) {
				rbList.addRemoteBinding(new RemoteBinding(entry.getKey().getTypeLiteral().getRawType(), entry.getValue().getProvider().get()));
			}
		}
		
		remoteBindings.lock();
	}
	
	@Override
	protected void startUp() throws Exception {
	
		socket = new ServerSocket();
		socket.bind(new InetSocketAddress(2401));			
	}

	@Override
	protected void run() throws Exception {
		
		System.out.println("Server created: " + socket.toString());
		while(enabled) {
			executor.execute(client.create(socket.accept(), remoteBindings));
		}		
	}
	
	@Override
	protected void triggerShutdown() {
		enabled = false;
	}
}
