package core.server.remote;

import java.util.Iterator;
import java.util.List;



import com.google.common.collect.Lists;

import core.remote.call.RemoteBinding;

public class RemoteBindingList implements Iterable<RemoteBinding> {
	private final List<RemoteBinding> bindings = Lists.newArrayList();
	private volatile boolean locked = false;
	
	public void addRemoteBinding(RemoteBinding binding) {
		if(locked) throw new RuntimeException("Remote Binding Class is LOCKED!!");
		bindings.add(binding);
	}
	
	public void lock() {
		locked = true;
	}

	@Override
	public Iterator<RemoteBinding> iterator() {
		return bindings.iterator();
	}

}
