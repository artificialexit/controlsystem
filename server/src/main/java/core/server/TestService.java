package core.server;


import com.google.common.util.concurrent.AbstractIdleService;
import core.RemoteTest;
import core.events.TestObject;

public class TestService extends AbstractIdleService implements RemoteTest {
	public TestService() {
	}

	@Override
	protected void shutDown() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void startUp() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void testCall() {
		System.out.println("Called remote object.....");
		
	}

	@Override
	public void post(Object object) {
		System.out.println("Called remote object with: " + object.toString());		
	}

	@Override
	public void tryMe(TestObject test) {
		test.thisWillFail();		
	}
}
