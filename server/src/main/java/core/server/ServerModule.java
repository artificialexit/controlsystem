package core.server;

import java.net.URL;
import java.util.List;


import com.google.common.util.concurrent.Service;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

import core.RemoteTest;
import core.SocketHandler;
import core.api.Remote;
import core.server.remote.RemoteBindingList;

public class ServerModule extends AbstractModule {

	@Override
	protected void configure() {
		//TestService serviceTester;
		//System.out.println("creating ServerModule..");
		Multibinder<Service> serviceBinder = Multibinder.newSetBinder(binder(), Service.class);
		serviceBinder.addBinding().to(Server.class).in(Singleton.class);
		//serviceBinder.addBinding().toInstance(serviceTester = new TestService());
		
		bind(RemoteBindingList.class).in(Singleton.class);
		
		bind(new TypeLiteral<List<URL>>(){}).annotatedWith(Names.named("classpath")).toProvider(new ClasspathProvider("server.classpath"));
		bind(new TypeLiteral<List<URL>>(){}).annotatedWith(Names.named("client.classpath")).toProvider(new ClasspathProvider("client.classpath"));
		
		install(new FactoryModuleBuilder()
	     .implement(SocketHandler.class, Client.class)
	     .build(ClientFactory.class));
		
		//Remote Bindings
		//bind(RemoteTest.class).annotatedWith(Remote.class).toInstance(serviceTester);
	}

}
