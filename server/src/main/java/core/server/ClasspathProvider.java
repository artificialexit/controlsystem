package core.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

class ClasspathProvider implements Provider<List<URL>> {
	private final String file;
	private List<URL> client = Lists.newArrayList();
	
	public ClasspathProvider(String file) {
		this.file = file;
	}

	@Override
	public List<URL> get() {
		List<URL> urls = Lists.newArrayList();
		System.out.println("Loading classpath from: " + file);
		try {
			BufferedReader plugins = new BufferedReader(new FileReader(file));
			String plugin = plugins.readLine();
			while(plugin != null) {
				System.out.println(plugin);
				urls.add(new URL(plugin));
				plugin = plugins.readLine();
			}
		} catch (NullPointerException e) {
			System.out.println("NullPointerException: " + e.getMessage());
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
		//System.out.println(urls);
		urls.addAll(client);

		return urls;
	}

	// Uncomment to run client plugins on the server	
	@Inject public void inject(@Named("client.classpath") List<URL> client) {
		try {
			String load = System.getProperty("core.client.plugins", "false");
			
			if("true".equals(load)) {
				if(!this.get().equals(client)) {
					this.client = client;
				}
			}
		} catch (NullPointerException e) {
			System.out.println("NullPointerException: " + e.getMessage());
		}
	}

}
