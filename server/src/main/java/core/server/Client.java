package core.server;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;

import core.events.BootClientEvent;
import core.events.ReadyForBindingsEvent;
import core.remote.RemoteSocketHandler;
import core.remote.call.RemoteBinding;
import core.server.remote.RemoteBindingList;


class Client extends RemoteSocketHandler {
	private final List<URL> urls;
	private final RemoteBindingList bindings;
	
	@Inject public Client(@Named("client.classpath") List<URL> urls, @Assisted RemoteBindingList bindings,  @Assisted Socket acceptSocket) {
		super(acceptSocket);
		this.urls = urls;
		this.bindings = bindings;
	}

	@Override
	protected void onConnect() {
		String hostAddr = getSocket().getLocalAddress().getHostAddress();

		System.out.println("Sending client classpath");

		//send client urls
		sendObject(transformUrls(urls, hostAddr));
	}
	
	@Subscribe public void clientReady(ReadyForBindingsEvent event) {
		System.out.println("Recieved ready event");
		System.out.println("Sending remote bindings");
		for(RemoteBinding binding : bindings) {			
			sendObject(binding);	
		}
		
		//boot client
		sendObject(new BootClientEvent());				
	}
	
	private static List<URL> transformUrls(List<URL> urls, String hostAddr) {
		List<URL> transformedUrls = Lists.newArrayList();
		
		System.out.println("Transforming urls");
		for(URL url : urls) {
			boolean local = false;
			//check if local addr
			try {
				local = InetAddress.getByName(url.getHost()).isLoopbackAddress();				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//if it is we transform it
			if(local) {
				URL trans;
				try {
					trans = new URL(url.getProtocol(), hostAddr, url.getPort(), url.getFile());
					transformedUrls.add(trans);
				} catch (MalformedURLException e) {
					System.out.println("Transforming url: " + url.toString() +" to host: "+hostAddr);
					e.printStackTrace();
				}	
			} else {
				transformedUrls.add(url);
			}
		}
		
		return transformedUrls;
	}
}
