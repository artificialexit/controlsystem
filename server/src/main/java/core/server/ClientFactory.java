package core.server;

import java.net.Socket;

import core.server.remote.RemoteBindingList;

interface ClientFactory {
	public Client create(Socket socket, RemoteBindingList remoteBindings);
}
