package core;

import java.io.*;
import java.util.*;

import com.google.common.collect.Maps;

public class Shell {
	private static final String GREETINGS = "Welcome to the System.%n";
	private static final String UNKNOWN_COMMAND = "Unknown command [%1$s]%n";
	private static final String COMMAND_ERROR = "Command error [%1$s]: [%2$s]%n";

	private static final String TIME_FORMAT = "%1$tH:%1$tM:%1$tS";
	private static final String PROMPT = TIME_FORMAT + " $ ";
	
	private static final Map<String, Action> actions = Maps.newHashMap();
	
	public static void main(String[] args) {
		Console console = System.console();
		installAction(new Exit());
		installAction(new List());
		
		if (console != null) {
			execCommandLoop(console);
		}
	}

	private static void execCommandLoop(final Console console) {
		console.printf(GREETINGS);
		while (true) {
			String commandLine = console.readLine(PROMPT, new Date());
			Scanner scanner = new Scanner(commandLine);

			if (scanner.hasNext()) {
				final String commandName = scanner.next().toUpperCase();

				try {
					Action action = actions.get(commandName);
					if(action == null)
						throw new IllegalArgumentException();
					

					String param = scanner.hasNext() ? scanner.next() : null;
					try {
						action.exec(console, new String[] { param });
					} catch(Throwable e) {
						console.printf(COMMAND_ERROR, commandName,
								e.getMessage());	
					}

				} catch (IllegalArgumentException e) {
					console.printf(UNKNOWN_COMMAND, commandName);
				}
			}

			scanner.close();
		}
	}
	
	public static void installAction(Action action) {
		System.out.println("Installing action: " + action.toString());
		actions.put(action.getName().toUpperCase(), action);
	}
	
	static class List implements Action {

		@Override
		public void exec(Console c, String[] params) throws Exception {
			for(String name : actions.keySet()) {
				c.printf("%s\n", name);
			}
		}

		@Override
		public String getName() {
			return "list";
		}
		
	}
}


class Exit implements Action {

	@Override
	public void exec(Console c, String[] params) throws Exception {
		System.exit(0);
	}

	@Override
	public String getName() {
		return "exit";
	}
	
}





