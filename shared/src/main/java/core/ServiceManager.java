package core;

import java.util.Set;



import com.google.common.collect.Sets;
import com.google.common.util.concurrent.Service;
import com.google.inject.Inject;

import core.events.MasterEventBus;

public class ServiceManager {
	private Set<Service> services = Sets.newHashSet();
	
	@Inject(optional=true) public void inject(Set<Service> services) {
		System.out.println("Injecting services");
		System.out.println(services);
		this.services = services;
	}
	
	public void registerServices(MasterEventBus bus) {
		for(Service service : services) {
			bus.register(service);
		}		
	}
	
	public void startServices() {
		for(Service service : services) {
			System.out.println("Starting.." + service.toString());
			service.startAndWait();
		}
	}

	public void stopServices() {
		for(Service service : services) {
			service.stop();
		}
	}
}

