package core;

import core.events.TestObject;

public interface RemoteTest {
	public void testCall();
	public void post(Object object);
	public void tryMe(TestObject test);
}
