package core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.PrintWriter;
import java.net.Socket;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class SocketHandler implements Runnable {

	private final Socket socket;
	private final EventBus bus = new EventBus();
	
	private final Debug debug = new Debug();
	
	private ObjectOutputStream out;
	private ObjectInputStream in;

	public SocketHandler(Socket acceptSocket) {
		socket = acceptSocket;
		bus.register(this);
		bus.register(debug);
	}

	@Override
	public void run() {
		System.out.println(this.getClass().getName() + " connected: " + getSocket().toString());
		connectOut();
		
		try {
			connectIn();
			
			if(getSocket().isConnected())
				onConnect();		
			
			while(getSocket().isConnected()) {
				try {
					Object obj = in.readObject();
					bus.post(obj);										
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			close();
		}
		System.out.println(this.getClass().getName() + " disconnected: " + getSocket().toString());
	}
	
	private void connectOut() {
		System.out.println("Connecting output stream");
		try {
			out = new ObjectOutputStream(new BufferedOutputStream(getSocket().getOutputStream()));
			out.flush();
		} catch (IOException e) {
			out = null;
		}	
	}
	
	private void connectIn() throws IOException {
		System.out.println("Connecting input stream");
		in = new ObjInStream(new BufferedInputStream(getSocket().getInputStream()));
	}
	
	private void close() {
		try {
			out.close();			
		} catch (IOException e1) {
			System.out.println("Failed to close output stream");
		} finally {
			out = null;
		}
		
		try {
			in.close();
			in = null;
		} catch (IOException e1) {
			System.out.println("Failed to close input stream");
		} finally {
			in = null;
		}
		
		try {
			getSocket().close();
		} catch (IOException e1) {
			System.out.println("Failed to close socket stream");
		}	
	}
	
	protected synchronized void sendObject(Object object) {
		if(out == null) return;
		
		try {
			out.reset();
			out.writeUnshared(object);
			debug.send(object);
			out.flush();
		} catch (IOException e) {
			System.out.println("Failed to send message: " + e.getMessage());
			close();
		}
	}
	
	
	@Subscribe
	public void deadEvents(DeadEvent event) {
		System.out.println("Unhandled object: " + event.getEvent().toString());
	}
	
	protected void onConnect() {}
	
	
	public Socket getSocket() {
		return socket;
	}


	static class ObjInStream extends ObjectInputStream {

		public ObjInStream(InputStream in) throws IOException {
			super(in);
		}
		
		@Override
		protected Class<?> resolveClass(ObjectStreamClass desc)
				throws IOException, ClassNotFoundException {
	        try {
	            return Class.forName(desc.getName(), false, Application.getClassLoader());
	        } catch (ClassNotFoundException ex) {
				return super.resolveClass(desc);
	        }
		}
		
	}
	
	class Debug {
		private static final String FILE = "socket.log";
		private PrintWriter output;
		public Debug() {
			FileWriter fos;
			try {
				fos = new FileWriter(FILE);
				output = new PrintWriter(fos, true);
				output.println();
				output.println();
				output.println();
				output.println(socket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void send(Object object) {
			log(">>> SEND: " + object.toString());

		}
		@Subscribe public void recv(Object object) {
			log("<<< RECV: " + object.toString());
		}
		
		public void log(String message) {
			output.println(message);
			//System.out.println(message);		
		}
	}
}
