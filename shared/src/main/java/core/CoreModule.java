package core;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class CoreModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(Application.Booter.class);
		bind(ServiceManager.class).in(Singleton.class);
	}

}
