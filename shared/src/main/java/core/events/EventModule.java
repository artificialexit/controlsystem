package core.events;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class EventModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(MasterEventBus.class).to(MasterEventsBusImpl.class).in(Singleton.class);
	}
}
