/**
 * 
 */
package core;


import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.ServiceLoader;



import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Named;

import core.events.MasterEventBus;
import core.events.ServicesStartedEvent;
import core.events.ShutdownEvent;

/**
 * @author mudien
 *
 */
public class Application {	
	public final static Thread THREAD = Thread.currentThread();
	
	public static void main(String[] args) {
		new Application();
	}
	
	public Application() {
		//Get some space.....
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("----------BOOTING APPLICATION-------------");
		
		System.out.println(Thread.currentThread());
			
		System.out.println("Stage 1: Building Initial Injector");		
		Injector stage1 =  Guice.createInjector(buildModules());
		System.out.println("Stage 2: Building ClassPath");		
		stage1.injectMembers(this);
		System.out.println("Stage 3: Building Final Injector");
		Injector injector = Guice.createInjector(buildModules());	
		System.out.println("Stage 4: Booting");
//		displayClassPath();
		boot(injector);

		startConsole();
	}
	
	private void startConsole() {
		Shell.main(new String[]{null});
	}

	@SuppressWarnings("unused")
	private void displayClassPath() {
        for(URL url : ((URLClassLoader) Thread.currentThread().getContextClassLoader()).getURLs())
        {
            System.out.println(url.getFile());
        }
	}
	
	private Iterable<Module> buildModules() {
		ServiceLoader<Module> modules =  ServiceLoader.load(Module.class, Thread.currentThread().getContextClassLoader());	
		return modules;		
	}
	
	@Inject public void buildClasspath(@Named("classpath") List<URL> urls) {
		//TODO possibly fix this nasty hack for swt...?
		String swt = "swt-gtk-linux-x86-3.7.1.jar";	
		if(System.getProperty("os.arch").contains("64")) {
			swt = "swt-gtk-linux-x86-64-3.7.1.jar";
		}
		List<URL> urlsToSkip = Lists.newArrayList();
		for(URL url : urls) {
			if(url.toString().contains("swt"))
				if(!url.toString().endsWith(swt))
					urlsToSkip.add(url);
		}
		for(URL url : urlsToSkip) {
			System.out.println("Rejecting: " + url.toString());
			urls.remove(url);
		}
		
		Thread.currentThread().setContextClassLoader(new URLClassLoader(urls.toArray(new URL[]{}), Thread.currentThread().getContextClassLoader()));
	}

	
	public void boot(Injector injector) {
		final Booter booter = injector.getInstance(Booter.class);
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {			
			@Override
			public void run() {
				booter.stop();			
			}
		}));
		
		booter.start();
	}	

	static class Booter extends AbstractIdleService {
		private final MasterEventBus bus;	
		private final ServiceManager serviceManager;
		
		@Inject public Booter(MasterEventBus bus, ServiceManager serviceManager) {
			this.bus = bus;
			this.serviceManager = serviceManager;
			this.bus.register(this);
		}

		@Override
		protected void shutDown() throws Exception {
			System.out.println("Shutting down....");
			serviceManager.stopServices();
			System.out.println("Shutdown Complete.");
		}

		@Override
		protected void startUp() throws Exception {
			System.out.println("Starting.....");
			serviceManager.registerServices(bus);
			serviceManager.startServices();
			bus.post(new ServicesStartedEvent());		
		}	
		
		@Subscribe
		public void shutdown(ShutdownEvent event) {
			System.exit(0);
		}
	}

	public static ClassLoader getClassLoader() {
		return THREAD.getContextClassLoader();
	}
}
