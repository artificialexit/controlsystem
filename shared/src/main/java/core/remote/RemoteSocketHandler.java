package core.remote;

import java.io.Console;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import core.Action;
import core.Shell;
import core.SocketHandler;
import core.remote.call.RemoteBinding;
import core.remote.call.RemoteCall;
import core.remote.call.RemoteObject;


public class RemoteSocketHandler extends SocketHandler {
	private final List<RemoteObject> objects;
	private final ExecutorService pool = Executors.newCachedThreadPool();
	
	public RemoteSocketHandler(Socket acceptSocket) {
		super(acceptSocket);
		
		//Synchronize the list
		List<RemoteObject> list = Lists.newArrayList();
		objects = Collections.synchronizedList(list);		
		
		Shell.installAction(new Action() {
			
			@Override
			public String getName() {
				return "remote:list";
			}
			
			@Override
			public void exec(Console c, String[] params) throws Exception {
				c.printf("%s\n", getSocket().toString());
				for(RemoteObject object : objects) {
					c.printf("%s\n", object.toString());
				}
			}
		});
	}
	
	public void handleRemoteCall() {
		
	}
	

	
	public void handleRemoteReturn() {
		
	}

/*******************************REMOTE   CALLS*********************************/
	@Subscribe
	public synchronized void remoteInvocation(RemoteCall call) {
		////System.out.println("RemoteInvocation: " + call.toString());
		
		//find object
		RemoteObject object = null;
		try {
			object = findRemoteObjectByUUID(call.getInstanceId());
			if(object == null)
				throw new RuntimeException("Unable to find instance");
			//System.out.println("Invoking method on: " + object.getInstance().toString());
		} catch(RuntimeException e) {
			System.out.println("Failed to find remote object IGNORING invokation");
			//System.out.println(objects.size());
			for(RemoteObject item : objects) {
				//System.out.println(item.getInstanceId());
			}
			
			return;
		}
		
		try {
			//System.out.println("Finding method");
			Method method = object.getMethod(call);
			//System.out.println("Calling method");
			method.invoke(object.getInstance(), transformRecievedObjects(call.getArgs()));
		} catch (Exception e) {
			System.out.println("**********************************************************************************");
			// TODO Auto-generated catch block
			//System.out.println("RemoteObject: " + object.toString());
			//System.out.println("RemoteInvocation: " + call.toString());
			//System.out.println("Failed to invoke method: " + e.getMessage());
			System.out.println(object.hashCode());
			for(Object items : objects) {
				System.out.println(items.hashCode());					
			}
			e.printStackTrace();
		}

	}	
	
	
	private Object[] transformSentObjects(Class<?>[] paramTypes, Object[] args) {
		if(args == null) return args;
		
		for(int i = 0, n = args.length; i < n; i++) {
			if(!(args[i] instanceof Serializable)) {
				args[i] = createRemoteObject(paramTypes[i], args[i]);
			}
		}
		
		return args;
	}
	private Object[] transformRecievedObjects(Object[] args) {
		if(args == null) return args;
		
		for(int i = 0, n = args.length; i < n; i++) {
			if(args[i] instanceof RemoteObject) {
				args[i] = getInstanceFromRemoteObject((RemoteObject) args[i]);
			}
		}
		
		return args;
	}
	
	private Object getInstanceFromRemoteObject(RemoteObject obj) {
		Object inst = findRemoteObject(obj).getInstance();
		
		if(inst == null)
			throw new NullPointerException("Couldn't find instance for remote object.");
		
		return inst;		
	}
	
	private RemoteObject createRemoteObject(Class<?> clazz, Object instance) {
		for(RemoteObject object : objects) {
			if(object.getInstance() == instance) {
				return object;
			}
		}
		
		RemoteObject object = RemoteObject.create(clazz, instance); 
		addRemoteObject(object);
		return object;
	}
	
	private RemoteObject findRemoteObject(RemoteObject obj) {
		RemoteObject search = findRemoteObjectByUUID(obj.getInstanceId());
		RemoteObject returnObject;
		if(search == null) {
			returnObject = addRemoteObject(obj);
		} else {
			returnObject = search;
		}
		return returnObject;
	}
	
	private RemoteObject findRemoteObjectByUUID(UUID uuid) {
		for(RemoteObject object : objects) {
			if(object.getInstanceId().equals(uuid)) {
				return object;
			}
		}
		
		return null;
	}
		
	protected RemoteObject addRemoteObject(RemoteObject e) {
		if(e.getInstance() == null) {
			//auto generate proxies for real remote objects
			e = RemoteObject.create(e, createCGProxy(e));
		}
		
		objects.add(e);		
		return e;
	}
	
/*******************************REMOTE BINDINGS********************************/
	@Override
	final protected synchronized void sendObject(Object object) {
		if(object instanceof RemoteBinding) {
			//hi jack all remote bindings for object cache			
			addRemoteObject((RemoteBinding)object);
		}
		
		super.sendObject(object);
	}
	
	class RemoteMethodInterceptor implements MethodInterceptor {
		private final UUID uuid;
		
		public RemoteMethodInterceptor(UUID uuid) {
			this.uuid = uuid;
		}

		@Override
		public Object intercept(Object proxy, Method method, Object[] args,
				MethodProxy methodProxy) throws Throwable {
			if(method.getName() == "toString") {
				return toString();
			}
			
			sendObject(RemoteCall.create(uuid, method, transformSentObjects(method.getParameterTypes(), args)));		
			//TODO remote returns fit in here somewhere....
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	final private <T> T createCGProxy(RemoteObject obj) {
		System.out.println("Creating proxy for: " + obj.toString());
        MethodInterceptor methodInterceptor =
                new RemoteMethodInterceptor(obj.getInstanceId());

            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(obj.getClazz());
            enhancer.setCallback(methodInterceptor);
            return (T)enhancer.create();	
	}
}
