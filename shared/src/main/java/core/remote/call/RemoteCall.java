package core.remote.call;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.UUID;

import com.google.common.base.Objects;

public class RemoteCall implements Serializable {
	private final UUID instanceId;
	private final String methodName;
	private final Class<?>[] parameterTypes;
	private final Object[] args;
	private final UUID callId;
	
	private RemoteCall(UUID instanceId, String methodName, Class<?>[] parameterTypes, Object[] args) {
		this.instanceId = instanceId;
		this.methodName = methodName;
		this.args = args;
		this.parameterTypes = parameterTypes;
		this.callId = UUID.randomUUID();
	}

	public UUID getInstanceId() {
		return instanceId;
	}

	public String getMethodName() {
		return methodName;
	}

	public Object[] getArgs() {
		return args;
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}
	
	public static RemoteCall create(UUID uuid, Method method, Object[] args) {
		return new RemoteCall(uuid, method.getName(), method.getParameterTypes(), args);
	}

	public UUID getCallId() {
		return callId;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Objects.toStringHelper(this).add("methodName", methodName).add("instanceId", instanceId)
				.add("callId", callId).toString();
	}
	
}
