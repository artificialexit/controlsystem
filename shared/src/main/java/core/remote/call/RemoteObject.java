package core.remote.call;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.UUID;

import com.google.common.base.Objects;


public class RemoteObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Class<?> clazz;
	private final UUID instanceId;
	private final transient Object instance;
	
	private <T> RemoteObject(Class<T> clazz, Object instance, UUID uuid) {
		if(instance == null)
			throw new RuntimeException("Creating RemoteObject with no instance!?");
		
		this.clazz = clazz;
		this.instanceId = uuid;
		this.instance = instance;
	}
	
	protected <T> RemoteObject(Class<T> clazz, Object instance) {
		this(clazz, instance, UUID.randomUUID());
	}

	public Class<?> getClazz() {
		return clazz;
	}

	@SuppressWarnings("unchecked")
	public <T> T getInstance() {
		return (T)instance;
	}

	public UUID getInstanceId() {
		return instanceId;
	}
	
	public static RemoteObject create(Object object) {
		return create(object.getClass(), object);
	}
	public static RemoteObject create(Class<?> clazz, Object object) {
		return new RemoteObject(clazz, object);
	}	
	public static RemoteObject create(RemoteObject object, Object proxy) {
		return new RemoteObject(object.getClazz(), proxy, object.getInstanceId());
	}
	
	public Method getMethod(RemoteCall call) throws NoSuchMethodException, SecurityException {
		return getClazz().getMethod(call.getMethodName(), call.getParameterTypes());
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("Class", clazz).add("InstanceId", instanceId).add("Instance", instance).toString();
	}
}
