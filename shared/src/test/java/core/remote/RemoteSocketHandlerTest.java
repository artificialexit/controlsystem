package core.remote;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.google.common.collect.Lists;

import core.remote.call.RemoteBinding;
import core.remote.call.RemoteCall;
import core.remote.call.RemoteObject;

public class RemoteSocketHandlerTest {
	RemoteSocketHandler handler;
	@Mock TestObject object;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		//create handler
		handler = spy(new RemoteSocketHandler(null));		
	}
	
	private RemoteObject setupRemoteObjectList() {
		//give it some remote objects
		List<RemoteObject> list = Lists.newArrayList();
		RemoteObject remoteObject = RemoteObject.create(object);
		list.add(remoteObject);
		
		//set the remote objects
		Whitebox.setInternalState(handler, "objects", list);	
		
		return remoteObject;
	}
	
	private ObjectOutputStreamStub getMockedStream() throws IOException {
		ObjectOutputStreamStub out = spy(new ObjectOutputStreamStub());
		Whitebox.setInternalState(handler, "out", out);
		return out;
	}
//	@Test
//	public void testRemoteSocketHandler() {
//		fail("Not yet implemented");
//	}
	@Test
	public void testRemoteInvocationWithRemoteObjectArgs() throws Exception {
		object = spy(new TestObjectStub());
		RemoteObject remoteObject = setupRemoteObjectList();
		ObjectOutputStreamStub out = getMockedStream();
		
		TestArg test = mock(TestArg.class);
		RemoteObject arg = RemoteObject.create(TestArg.class, test);
		
		//run the test
		Method method = object.getClass().getDeclaredMethod("testCall", new Class[]{ TestArg.class }); //get args from test class
		handler.remoteInvocation(RemoteCall.create(remoteObject.getInstanceId(), method, new Object[]{ arg })); //run with remote object
		
		verify(object).testCall(any(TestArg.class));
		verify(out).writeUnshared(any());
		
		
		assertTrue(out.getLastObject() instanceof RemoteCall);
	}
	

	@Test
	public void testRemoteInvocationWithSerializableArgs() throws NoSuchMethodException, SecurityException {		
		RemoteObject remoteObject = setupRemoteObjectList();
		
		TestSerializableArg arg = new TestSerializableArg();
		
		//run the test
		Method method = object.getClass().getDeclaredMethod("testCall", new Class[]{ arg.getClass() });
		handler.remoteInvocation(RemoteCall.create(remoteObject.getInstanceId(), method, new Object[]{ arg }));
		
		verify(object).testCall(arg);
	}
	
	@Test
	public void testRemoteInvocationNoArgs() throws NoSuchMethodException, SecurityException {
		RemoteObject remoteObject = setupRemoteObjectList();	
		
		//run the test
		Method method = object.getClass().getDeclaredMethod("testCall");
		handler.remoteInvocation(RemoteCall.create(remoteObject.getInstanceId(), method, null));
		
		verify(object).testCall();		
	}

//	@Test
//	public void testHandleRemoteReturn() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetRemoteModule() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testHandleRemoteCall() {
//		fail("Not yet implemented");
//	}
//
	@Test
	public void testAddRemoteBinding() {
		TestObject object = mock(TestObject.class);
		RemoteBinding binding = new RemoteBinding(TestObject.class, object);
		handler.sendObject(binding);
		
		@SuppressWarnings("unchecked")
		List<RemoteObject> list = (List<RemoteObject>) Whitebox.getInternalState(handler, "objects");
		
		assertTrue(list.size() == 1);			
		assertEquals(list.iterator().next(), binding);
	}
	
	
	static interface TestObject {
		void testCall();
		void testCall(TestArg arg);
		void testCall(TestSerializableArg arg);
	}
	
	static class TestSerializableArg implements Serializable {
		private static final long serialVersionUID = 1L;		
	}
	static interface TestArg {
		void testCall();
	}
	
	static class TestObjectStub implements TestObject {

		@Override
		public void testCall() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void testCall(TestArg arg) {
			System.out.println("Calling function on remote object");
			arg.testCall();
		}

		@Override
		public void testCall(TestSerializableArg arg) {
		}
		
	}
	
	static class ObjectOutputStreamStub extends ObjectOutputStream {
		Object lastObject = null;
		
		public ObjectOutputStreamStub() throws IOException {
			super(mock(OutputStream.class));
		}
		@Override
		public void reset() throws IOException {
			System.out.println("reset CALLED");
		}		
		@Override
		public void writeUnshared(Object obj) throws IOException {
			System.out.println("writeUnshared CALLED");
			lastObject = obj;
		}		
		@Override
		public void flush() throws IOException {
			System.out.println("flush CALLED");
			
		}
		public Object getLastObject() {
			return lastObject;
		}		
		
	}
}
