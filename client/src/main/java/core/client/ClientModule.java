package core.client;

import java.net.URL;
import java.util.List;

import com.google.common.util.concurrent.Service;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

public class ClientModule extends AbstractModule {

	@Override
	protected void configure() {
//		Multibinder<Service> serviceBinder = Multibinder.newSetBinder(binder(), Service.class);
//		serviceBinder.addBinding().to(TestService.class).in(Singleton.class);

		bind(new TypeLiteral<List<URL>>(){}).annotatedWith(Names.named("classpath")).toProvider(ClasspathProvider.class);
		
		bind(Client.class).toInstance(Client.getInstance());
		
		install(Client.getModule());
	}

}
