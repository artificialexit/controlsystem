package core.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.google.inject.AbstractModule;

public class Client extends AbstractExecutionThreadService {
	private static Client INSTANCE = new Client();
	private Socket socket;
	private ClientHandler handler;
	private Thread handlerThread;
	
	private Client() {}
	
	public static Client getInstance() {
		return INSTANCE;
	}
	
	public static com.google.inject.Module getModule() {
		return INSTANCE.new Module();
	}
	
	class Module extends AbstractModule {

		@Override
		protected void configure() {
			if(handler != null)
				handler.setupBindings(binder());			
		}
		
	}
	
	@Override
	protected void startUp() throws Exception {
		handlerThread = Thread.currentThread();
		handler = new ClientHandler(socket = new Socket());
		try {
			socket.connect(new InetSocketAddress(System.getProperty("core.server.addr", "nathan-work"), 2401));
		} catch (IOException e) {
			System.out.println("Socket failed to connect: " + e.getMessage());
			stop();
		}
	}
	
	@Override
	protected void shutDown() throws Exception {
		System.out.println("Shutting down.");
		System.exit(0);
	}

	@Override
	protected void run() throws Exception {
		try {
			handler.run();
		} catch(Throwable e) {
			System.out.println("Handler Exception: ");
			e.printStackTrace();
		}
	}
	
	public List<URL> getClassPath() {
		System.out.println("Waiting to receive class path...");
		while(!handler.hasClassPath()) {
			try {
				TimeUnit.MILLISECONDS.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(state() != State.RUNNING) {
				//throw new RuntimeException("Service failed.. cannot get classpath");				
				System.out.println("Service failed.. cannot get classpath");
				break;
			}
		}
		
		return handler.getClassPath();
	}
	
	public void setCL(ClassLoader cl) {
		handlerThread.setContextClassLoader(cl);
	}
}
