package core.client;


import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Inject;
import core.RemoteTest;
import core.api.Remote;
import core.events.ShutdownEvent;
import core.events.TestObject;

public class TestService extends AbstractIdleService {
	private RemoteTest bus;
	
	@Inject(optional=true) public void inject(@Remote RemoteTest bus) {
		System.out.println("Injecting RemoteTest");
		System.out.println(bus);
		this.bus = bus;
	}

	@Override
	protected void shutDown() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	protected void startUp() throws Exception {
		if(bus != null) {
			bus.testCall();
			bus.post(new ShutdownEvent());
			
			bus.post(new TestObject());
			
			bus.tryMe(new TestObject());
		}
	}

}
