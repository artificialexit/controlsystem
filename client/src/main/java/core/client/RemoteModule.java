package core.client;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provider;
import core.api.Remote;

public class RemoteModule implements Module {
	@SuppressWarnings("rawtypes")
	Map<Class<?>, Provider> remoteClasses = Maps.newHashMap();
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void configure(Binder binder) {
		for(Entry<Class<?>, Provider> entry : remoteClasses.entrySet()) {
			binder.bind(entry.getKey()).annotatedWith(Remote.class).toProvider(entry.getValue());
		}
	}
	
	public void addRemoteInterface(Class<?> clazz, Provider<?> provider) {
		remoteClasses.put(clazz, provider);
	}
}
