package core.client;

import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;


import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Binder;
import com.google.inject.Provider;

import core.events.BootClientEvent;
import core.events.ReadyForBindingsEvent;
import core.remote.RemoteSocketHandler;
import core.remote.call.RemoteBinding;
import core.remote.call.RemoteObject;

public class ClientHandler extends RemoteSocketHandler {
	private volatile boolean classpath = false;
	private volatile boolean boot = false;
	
	private List<URL> urls = Lists.newArrayList();
		
	public ClientHandler(Socket acceptSocket) {
		super(acceptSocket);
		// TODO Auto-generated constructor stub
	}
	
	public List<URL> getClassPath() {		
		return urls;
	}
	
	public boolean hasClassPath() {
		return classpath;
	}
	
	@Subscribe public void boot(BootClientEvent event) {
		System.out.println("Booting client");
		boot = true;
	}
	
	@Subscribe public void setClassPath(List<URL> urls) {
		this.urls = urls;
		classpath = true;
	}

	public void setupBindings(Binder binder) {		
		System.out.println("Stalling for remote bindings, Sending ready event");
		sendObject(new ReadyForBindingsEvent());
		try {
			while(!boot)
				TimeUnit.MILLISECONDS.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Install remote bindings");
		binder.install(remote);
	}
	
	RemoteModule remote = new RemoteModule();
	@SuppressWarnings({ "rawtypes" })
	@Subscribe
	public void handleRemoteBinding(RemoteBinding binding) {
		RemoteObject o = addRemoteObject(binding);
		System.out.println("Recieved remote binding: " + binding.toString());		
		remote.addRemoteInterface(o.getClazz(), new ProxyProvider(o));
	}	
	
	class ProxyProvider<T> implements Provider<T> {
	    private final RemoteObject binding;

		public ProxyProvider(RemoteObject o) {
			this.binding = o;
		}

		@Override
		public T get() {
			return binding.getInstance();
		}
	}
}


