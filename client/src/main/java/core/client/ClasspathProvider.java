package core.client;

import java.net.URL;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class ClasspathProvider implements Provider<List<URL>> {
	private final Client client;
	
	@Inject public ClasspathProvider(Client client) {
		this.client = client;
		this.client.startAndWait();
		System.out.println(this.client);
	}

	@Override
	public List<URL> get() {
		return client.getClassPath();
	}
}
