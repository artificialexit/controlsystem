package core.events;

public interface MasterEventBus {
	public void post(Object event);
	public void register(Object object);
	public void unregister(Object object);
}
