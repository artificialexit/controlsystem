package core.events;

public interface EventListener {
	public void handleEvent(Object event);
}
