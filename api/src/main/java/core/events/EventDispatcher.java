package core.events;

public interface EventDispatcher {
	public void addListener(EventListener listener);
	public void removeListener(EventListener listener);
	
	public void postEvent(Object object);
}
